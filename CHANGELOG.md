# Change Log
All notable changes to this project will be documented in this file.

## [Unreleased]
- empty

## [1.0.1] - 2021-05-21
### Added
- empty
### Fixed
- upgrade API version to 5.

## [1.0.0] - 2021-04-16
### Added
- The initial release.

[1.0.0]: https://gitee.com/ts_ohos/androidsvg-for-ohos/tree/v1.0.0
[1.0.1]: https://gitee.com/ts_ohos/androidsvg-for-ohos/tree/v1.0.1