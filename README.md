# androidsvg
本项目是基于开源项目**BigBadaboom/androidsvg**进行适用harmonyos的移植和开发的，可以通过项目标签以及 github地址[https://github.com/BigBadaboom/androidsvg](https://github.com/BigBadaboom/androidsvg) 追踪到原项目

移植版本：Release_1.4

## 1. 项目介绍
### 项目名称：androidsvg
### 所属系列：harmonyos的第三方组件适配移植
### 功能：
1. 实现svg格式图片的解析和渲染功能；
2. 支持更多的svg element和css样式；

### 项目移植状态：
1. 主要功能已经移植；
2. 添加了demo模块用于效果展示；

### 调用差异：暂无
### 原项目Doc地址：[http://bigbadaboom.github.io/androidsvg/index.html](http://bigbadaboom.github.io/androidsvg/index.html)
### 编程语言：java

## 2. 集成指引
### 方式一
1. 下载或自行编译生成androidsvg的.har文件，文件路径为：./demo/libs/ohossvg-debug.har。
2. 自行编译时，需要注意要自行添加签名。
3. 导入你的harmonyos项目模块的**./libs**中。
4. 在模块下的**build.gradle**中确认依赖**./libs**下的.har包，``implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])``。
5. 在代码中使用。

### 方式二：
1. 在根目录的`build.gradle`文件中添加`mavenCentral()`：
   ```groovy
   // Top-level build file where you can add configuration options common to all sub-projects/modules.
   ...
   buildscript {
       repositories {
           ...
           mavenCentral()
       }
       ...
   }
   
   allprojects {
       repositories {
           ...
           mavenCentral()
       }
   }
   ```

2. 在module目录下的`build.gradle`文件中添加``

   ```groovy
   ...
   dependencies {
       ...
       implementation 'com.gitee.ts_ohos:androidsvg-for-ohos:1.0.1'
   }
   ```

## 3. 使用说明
更多用法参考本项目的**demo**，原项目**Java Doc**与**OHOS**的文档。
**Java doc : ** [https://gitee.com/ts_ohos/androidsvg-for-ohos/tree/ohos_main/doc](https://gitee.com/ts_ohos/androidsvg-for-ohos/tree/ohos_main/doc)
主要用法如下：
   ```java
/** SVG Reading */

/** Generate SVG obj from InputStream. */
public static SVG getFromInputStream(java.io.InputStream is)
    throws SVGParseException

/** Generate SVG obj from svg String. */
public static SVG getFromString(java.lang.String svg)
    throws SVGParseException
 
/** Generate SVG obj from hap resource. */
public static SVG getFromResource(Context context, int resourceId)
    throws SVGParseException

/** Generate SVG obj from hap rawfile resource. */ 
public static SVG getFromAsset(ResourceManager resManager, String filename)
    throws SVGParseException, IOException
   ```

```java
/** SVG Rendering */

/** Render SVG obj to a Picture obj. */
public void renderToCanvas(Canvas canvas) 
public void renderToCanvas(Canvas canvas, RectF viewPort)

/** Render SVG obj to a Canvas obj. */
public void renderToCanvas(Canvas canvas)
public void renderToCanvas(Canvas canvas, RectF viewPort)
```

```java
/** Resolving references to external files. */
public class CachingImageLoader extends SVGExternalFileResolver {
    private static final String RESOURCE_PRE_FIX = "resources/rawfile/";
    private ResourceManager assetManager;
    private HashMap<String, PixelMap> cache = new HashMap<>();

    /**
     * CachingImageLoader constructor
     *
     * @param assetManager ResourceManager obj
     */
    public CachingImageLoader(ResourceManager assetManager) {
        super();
        this.assetManager = assetManager;
    }

    /**
     * Attempt to find the specified image file in the "assets" folder and return a decoded Bitmap.
     */
    @Override
    public PixelMap resolveImage(String filename) {
        String rawFileName = RESOURCE_PRE_FIX + filename;

        // Ignore invalid requests
        if (rawFileName.isEmpty()) {
            return null;
        }

        // Check file is in cache first
        if (cache.containsKey(rawFileName)) {
            return cache.get(rawFileName);
        }
        // Try loading it from assets folder
        try {
            // Get the image as an InputStream
            InputStream istream = assetManager.getRawFileEntry(rawFileName).openRawFile();

            // Attempt to decode it
            PixelMap image = ImageSource.create(istream, new ImageSource.SourceOptions())
                    .createPixelmap(new ImageSource.DecodingOptions());

            // Store it in the cache
            cache.put(rawFileName, image);
            return image;
        } catch (IOException ex) {
            return null;
        }
    }
}
```

## 4. 效果演示
**1. 解析和渲染svg到pixelmap**
**2. 解析和渲染svg，添加css样式**
**3. 解析和渲染svg，并使用SVGImgeView显示，结合AnimatorValue和css样式增加动画效果**
**4. 解析和渲染svg，并使用SVGImgeView显示，结合AnimatorValue和css样式增加动画效果。并在svg中解析引用的其他静态图片。**

**\*注 : demo中的图片透明度渐变效果暂无效果**

<img src="./pic/demo_1.gif" width="200"><img src="./pic/demo_2.gif" width="200"><img src="./pic/demo_3.gif" width="200"><img src="./pic/demo_4.gif" width="200">

## 5. 版本迭代
- v1.0.0 基于原项目最新版本，初次提交。
- v1.0.1 更新harmonyos API至5。

## 6. 版本和许可信息
- Apache License 2.0
- [https://gitee.com/ts_ohos/androidsvg-for-ohos/blob/ohos_main/LICENSE](https://gitee.com/ts_ohos/androidsvg-for-ohos/blob/ohos_main/LICENSE)

```
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```