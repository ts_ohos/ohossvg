package com.caverock.ohossvg.demo.slice;

import com.caverock.ohossvg.demo.ResourceTable;

import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.webengine.ResourceRequest;
import ohos.agp.components.webengine.ResourceResponse;
import ohos.agp.components.webengine.WebAgent;
import ohos.agp.components.webengine.WebView;
import ohos.agp.utils.TextTool;
import ohos.global.resource.Resource;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.net.Uri;

import java.io.IOException;
import java.net.URLConnection;

public class AboutAbilitySlice extends BaseSlice {
    private static final HiLogLabel LOG_LABEL = new HiLogLabel(HiLog.LOG_APP, 0x101, "AboutAbilitySlice");

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        Component view
                = LayoutScatter.getInstance(this)
                .parse(ResourceTable.Layout_ability_slice_main_about,
                        null,
                        false);
        if (!(view instanceof ComponentContainer)) {
            return;
        }
        ComponentContainer rootLayout = (ComponentContainer) view;
        WebView webView = new WebView(this);
        view = rootLayout.findComponentById(ResourceTable.Id_web_container);
        if (view instanceof ComponentContainer) {
            ComponentContainer videoPlayLayout = (ComponentContainer) view;
            videoPlayLayout.addComponent(webView);
        }
        super.setUIContent(rootLayout);
        webView.setWebAgent(new WebAgent() {
            @Override
            public ResourceResponse processResourceRequest(WebView webView, ResourceRequest request) {
                final String authority = "about.com";
                final String rawFile = "/rawfile/";
                Uri requestUri = request.getRequestUrl();
                if (authority.equals(requestUri.getDecodedAuthority())) {
                    String path = requestUri.getDecodedPath();
                    if (TextTool.isNullOrEmpty(path)) {
                        return super.processResourceRequest(webView, request);
                    }
                    if (path.startsWith(rawFile)) {
                        // 根据自定义规则访问资源文件
                        String rawFilePath = "resources/rawfile/" + path.replace(rawFile, "");
                        String mimeType = URLConnection.guessContentTypeFromName(rawFilePath);
                        try {
                            Resource resource = getResourceManager().getRawFileEntry(rawFilePath).openRawFile();
                            return new ResourceResponse(mimeType, resource, null);
                        } catch (IOException e) {
                            HiLog.info(LOG_LABEL, "open raw file failed");
                        }
                    }
                }
                return super.processResourceRequest(webView, request);
            }
        });

        webView.load("https://about.com/rawfile/about.html");
    }
}
