package com.caverock.ohossvg.demo.slice;

import com.caverock.ohossvg.SVGImageView;
import com.caverock.ohossvg.demo.ResourceTable;
import com.caverock.ohossvg.demo.utils.RubiksCube;

import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;

import java.security.SecureRandom;
import java.util.Locale;

public class ChangingColorAbilitySlice extends BaseSlice {
    private static final int BRICK_TWO = 2;
    private static final int BRICK_THREE = 3;
    private static final int FACE_NINE = 9;

    // Cube image routines
    private static String[] faceColours
            = new String[]{"#f9ca2e", "#c72e33", "#297537", "#f36021", "#255ea2", "#ffffff"};
    SVGImageView lego = null;
    SVGImageView cube = null;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_slice_changing_color);

        lego = (SVGImageView) findComponentById(ResourceTable.Id_lego);
        lego.setClickedListener(new LegoClickListener());

        cube = (SVGImageView) findComponentById(ResourceTable.Id_cube);
        cube.setClickedListener(new CubeClickListener());
    }

    // Lego image routines
    private class LegoClickListener implements Component.ClickedListener {
        String[] brickColours = new String[]{"#1698ec", "#fef134", "#95e881", "#ea272e"};

        @Override
        public void onClick(Component v) {
            shuffleArray(brickColours);
            String css = String.format(Locale.ROOT,
                    " .brick1 { fill:%s; } .brick2 { fill:%s; } .brick3 { fill:%s; }.brick4 { fill:%s; } ",
                    brickColours[0], brickColours[1], brickColours[BRICK_TWO], brickColours[BRICK_THREE]);
            ChangingColorAbilitySlice.this.lego.setCSS(css);
        }
    }

    // Fisher-Yates shuffle
    static void shuffleArray(String[] array) {
        SecureRandom rnd = new SecureRandom();
        for (int i = array.length - 1; i > 0; i--) {
            int index = rnd.nextInt(i + 1);

            // Simple swap
            String temp = array[index];
            array[index] = array[i];
            array[i] = temp;
        }
    }

    private class CubeClickListener implements Component.ClickedListener {
        @Override
        public void onClick(Component v) {
            RubiksCube cube = RubiksCube.getInstance();
            cube.shuffleOneStep();
            updateCubeCSS(cube);
        }
    }

    private void updateCubeCSS(RubiksCube cube) {
        StringBuilder css = new StringBuilder();
        doFace(css, cube, RubiksCube.TOP, 'T');
        doFace(css, cube, RubiksCube.LEFT, 'L');
        doFace(css, cube, RubiksCube.RIGHT, 'R');
        ChangingColorAbilitySlice.this.cube.setCSS(css.toString());
    }

    private void doFace(StringBuilder css, RubiksCube cube, int face, char cssClassTilePrefix) {
        for (int tile = 0; tile < FACE_NINE; tile++) {
            css.append(".");
            css.append(cssClassTilePrefix);
            css.append(tile);
            css.append(" {fill: ");
            css.append(faceColours[cube.getColour(face, tile)]);
            css.append("} ");
        }
    }
}
