package com.caverock.ohossvg.demo.slice;

import com.caverock.ohossvg.SVGImageView;
import com.caverock.ohossvg.demo.ResourceTable;

import ohos.aafwk.content.Intent;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Text;

public class LineDrawingAbilitySlice extends BaseSlice {
    // milliseconds
    private static final int ANIM_DURATION = 3000;
    // the total length of the doodle path
    private static final int ANIM_PATH_LENGTH = 3018;
    private boolean mFlashBack = false;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_slice_line_drawing);
        final Text title = (Text) findComponentById(ResourceTable.Id_title);
        final SVGImageView doodle = (SVGImageView) findComponentById(ResourceTable.Id_svg_image);

        title.setText("Animated line drawing using stroke-dashoffset");

        // Hide line when first displayed
        doodle.setCSS("#doodle { opacity: 0; }");
        doodle.setImageAsset("7doodle.svg");

        AnimatorValue av = new AnimatorValue();
        av.setDuration(ANIM_DURATION);
        av.setLoopedCount(Animator.INFINITE);

        av.setLoopedListener(animator -> mFlashBack = !mFlashBack);

        av.setValueUpdateListener((animatorValue, vv) -> {
            // Calculate the correct stroke-dashoffset at this point in the animation (ANIM_PATH_LENGTH -> 0)
            int dashOffset = Math.round(ANIM_PATH_LENGTH * (mFlashBack ? vv : (1 - vv)));
            // Update the CSS of the SVGImageView. Will result in the SVG being updated.
            String css = String.format("#doodle { stroke-dasharray: %d %d; stroke-dashoffset: %d; }",
                    ANIM_PATH_LENGTH, ANIM_PATH_LENGTH, dashOffset);
            doodle.setCSS(css);
        });
        av.start();
    }
}
