package com.caverock.ohossvg.demo.slice;

import com.caverock.ohossvg.SVG;
import com.caverock.ohossvg.SVGImageView;
import com.caverock.ohossvg.demo.ResourceTable;
import com.caverock.ohossvg.demo.utils.CachingImageLoader;

import ohos.aafwk.content.Intent;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Text;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class LineImageAbilitySlice extends BaseSlice {
    private static final int ANIM_DURATION = 1500; // milliseconds
    private static final float ANIM_LINES_DRAW_END = 0.9f;
    private static final float ANIM_IMAGE_FADE_START = 0.8f;
    private static final int PATH_LENGTH_PLATE_LG = 2794;
    private static final int PATH_LENGTH_PLATE_SM = 1903;
    private static final int PATH_LENGTH_SVG = 903;
    private static final int HILOG_DOMAIN = 0x666;

    private static final HashMap<String, Integer> pathLengths;

    static {
        // Map of path lengths for all the shapes in the svg we want to draw
        pathLengths = new HashMap<>();
        pathLengths.put("#plate-lg", PATH_LENGTH_PLATE_LG);
        pathLengths.put("#plate-sm", PATH_LENGTH_PLATE_SM);
        pathLengths.put(".orange", PATH_LENGTH_SVG); // largest of all the orange paths
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_slice_line_image);

        final Text title = (Text) findComponentById(ResourceTable.Id_title);
        final SVGImageView oranges = (SVGImageView) findComponentById(ResourceTable.Id_svg_image);

        title.setText(System.lineSeparator() + "Another animated line drawing example." + "Tap to restart animation.");
        SVG.registerExternalFileResolver(new CachingImageLoader(getResourceManager()));
        oranges.setCSS("ellipse, path, image { opacity: 0; }"); // Hide line when first displayed
        oranges.setImageAsset("oranges.svg");
        oranges.setClickedListener(component -> beginAnimation(oranges));

        beginAnimation(oranges);
    }

    private void beginAnimation(final SVGImageView oranges) {
        AnimatorValue av = new AnimatorValue();
        av.setDuration(ANIM_DURATION);
        av.setValueUpdateListener((animatorValue, v) -> {
            StringBuilder sb = new StringBuilder();

            float imageOpacity = mapRangeClamped(v, ANIM_IMAGE_FADE_START, 1f, 0f, 1f);
            sb.append(String.format(Locale.ROOT, "#image { opacity: %.2f; } ", imageOpacity));

            // Loop through all the defined paths in the pathLengths map, calculate the dash offset
            // for this point in time, and update the CSS for the SVGImageView
            for (Map.Entry<String, Integer> entry : pathLengths.entrySet()) {
                // Calculate the correct stroke-dashoffset at this point in the animation (ANIM_PATH_LENGTH -> 0)
                int pathLength = entry.getValue();
                int dashOffset = Math.round(mapRangeClamped(v, 0f, ANIM_LINES_DRAW_END, (float) pathLength, 0));
                sb.append(String.format("%s { stroke-dasharray: %d %d; stroke-dashoffset: %d; } ",
                        entry.getKey(), pathLength, pathLength, dashOffset));
            }

            // Update the CSS of the SVGImageView. It will result in the SVG being updated.
            oranges.setCSS(sb.toString());
        });
        av.start();
    }

    /*
     * Map a value in the range [inMin..inMax] to the range [outMin..outMax].
     * Assumes that inMin <= inMax
     */
    private float mapRangeClamped(Float value, float inMin, float inMax, float outMin, float outMax) {
        value = Math.max(inMin, Math.min(inMax, value));
        float inRange = inMax - inMin;
        float outRange = outMax - outMin;
        return outMin + (value - inMin) * outRange / inRange;
    }
}
