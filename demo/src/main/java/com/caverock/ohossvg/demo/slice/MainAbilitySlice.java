/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.caverock.ohossvg.demo.slice;

import com.caverock.ohossvg.demo.ResourceTable;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        findComponentById(ResourceTable.Id_btn_main_home)
                .setClickedListener(component -> present(new HomeAbilitySlice(), new Intent()));

        findComponentById(ResourceTable.Id_btn_main_about)
                .setClickedListener(component -> present(new AboutAbilitySlice(), new Intent()));

        findComponentById(ResourceTable.Id_btn_renderer_image)
                .setClickedListener(component -> present(new RendererImageAbilitySlice(), new Intent()));

        findComponentById(ResourceTable.Id_btn_changing_color)
                .setClickedListener(component -> present(new ChangingColorAbilitySlice(), new Intent()));

        findComponentById(ResourceTable.Id_btn_line_drawing)
                .setClickedListener(component -> present(new LineDrawingAbilitySlice(), new Intent()));

        findComponentById(ResourceTable.Id_btn_line_image)
                .setClickedListener(component -> present(new LineImageAbilitySlice(), new Intent()));
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
