package com.caverock.ohossvg.demo.slice;

import com.caverock.ohossvg.SVG;
import com.caverock.ohossvg.SVGParseException;
import com.caverock.ohossvg.demo.ResourceTable;

import ohos.aafwk.content.Intent;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.render.Canvas;
import ohos.agp.render.Texture;
import ohos.agp.utils.Color;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;

import java.io.IOException;
import java.security.SecureRandom;

public class RendererImageAbilitySlice extends BaseSlice {
    private static final String RESOURCE_PRE_FIX = "resources/rawfile/";
    private static final HiLogLabel LOG_LABEL
            = new HiLogLabel(HiLog.LOG_APP, 0x101, RendererImageAbilitySlice.class.getSimpleName());

    private static final int WIDTH = 1200;
    private static final int HEIGHT = 1200;
    private static final int NUM_BOUQUETS = 10;
    private static final float SCALE_MIN = 0.1f;
    private static final float SCALE_MAX = 0.5f;
    private static final int COLOR_NUM = 255;
    private static final int SCALE_PART = 2;

    SVG bouquet = null;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_slice_renderer_image);
        final Text titleView = (Text) findComponentById(ResourceTable.Id_title);
        final Image imageView = (Image) findComponentById(ResourceTable.Id_image);

        titleView.setText(ResourceTable.String_title_render_to_image);

        // Load the SVG from the assets folder
        try {
            bouquet = SVG.getFromAsset(getAbility().getResourceManager(), RESOURCE_PRE_FIX + "cherry.svg");
        } catch (SVGParseException | IOException e) {
            HiLog.error(LOG_LABEL,
                    "Could not load flower emoji SVG : "
                            + ((e instanceof SVGParseException) ? e.getMessage() : ""));
        }

        // Se tab listener
        imageView.setClickedListener(c -> {
            PixelMap generatedImage = generateImage();
            imageView.setPixelMap(generatedImage);
        });

        PixelMap generatedImage = generateImage();
        imageView.setPixelMap(generatedImage);
    }

    private PixelMap generateImage() {
        PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
        options.size = new Size(WIDTH, HEIGHT);
        options.pixelFormat = PixelFormat.ARGB_8888;
        options.editable = true;
        PixelMap pixelMap = PixelMap.create(options);

        Texture texture = new Texture(pixelMap);
        Canvas canvas = new Canvas(texture);

        canvas.drawColor(Color.rgb(COLOR_NUM, COLOR_NUM, COLOR_NUM), Canvas.PorterDuffMode.DST_IN);

        SecureRandom random = new SecureRandom();
        for (int i = 0; i < NUM_BOUQUETS; i++) {
            // Save the canvas state (because we are going to alter the transformation matrix)
            canvas.save();

            // Choose a random scale
            // If we don't specify a viewport box, then OhosSVG will use the bounds of the Canvas as the viewport.
            // So a scale of 1.0 corresponds to that size. A scale of 0.1 would draw the SVG at 1/10th of the size of
            // the Canvas/Bitmap.
            float scale = SCALE_MIN + random.nextFloat() * (SCALE_MAX - SCALE_MIN);

            // Choose a random position
            float xx = random.nextFloat(); // 0.0 -> 1.0
            float yy = random.nextFloat();

            // If we just draw the SVG at x,y, then it will be drawn below and to the right of that.
            // But we want to centre at x,y instead. We can do that by subtracting half the scale.
            xx -= scale / SCALE_PART;
            yy -= scale / SCALE_PART;

            canvas.translate(xx * WIDTH, yy * HEIGHT);
            canvas.scale(scale, scale);

            // Now render the SVG to the Canvas
            bouquet.renderToCanvas(canvas);

            // And restore the Canvas's matrix to what it was before we altered it
            canvas.restore();
        }
        return pixelMap;
    }
}
