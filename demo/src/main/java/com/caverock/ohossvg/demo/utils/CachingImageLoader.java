package com.caverock.ohossvg.demo.utils;

import com.caverock.ohossvg.SVGExternalFileResolver;

import ohos.global.resource.ResourceManager;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

public class CachingImageLoader extends SVGExternalFileResolver {
    private static final String RESOURCE_PRE_FIX = "resources/rawfile/";
    private ResourceManager assetManager;
    private HashMap<String, PixelMap> cache = new HashMap<>();

    /**
     * CachingImageLoader constructor
     *
     * @param assetManager ResourceManager obj
     */
    public CachingImageLoader(ResourceManager assetManager) {
        super();
        this.assetManager = assetManager;
    }

    /**
     * Attempt to find the specified image file in the "assets" folder and return a decoded Bitmap.
     */
    @Override
    public PixelMap resolveImage(String filename) {
        String rawFileName = RESOURCE_PRE_FIX + filename;

        // Ignore invalid requests
        if (rawFileName.isEmpty()) {
            return null;
        }

        // Check file is in cache first
        if (cache.containsKey(rawFileName)) {
            return cache.get(rawFileName);
        }
        // Try loading it from assets folder
        try {
            // Get the image as an InputStream
            InputStream istream = assetManager.getRawFileEntry(rawFileName).openRawFile();

            // Attempt to decode it
            PixelMap image = ImageSource.create(istream, new ImageSource.SourceOptions())
                    .createPixelmap(new ImageSource.DecodingOptions());

            // Store it in the cache
            cache.put(rawFileName, image);
            return image;
        } catch (IOException ex) {
            return null;
        }
    }
}
