/*
   Copyright 2013 Paul LeBeau, Cave Rock Software Ltd.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package com.caverock.ohossvg;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Picture;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.app.dispatcher.TaskDispatcher;
import ohos.app.dispatcher.task.TaskPriority;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.global.resource.NotExistException;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.net.Uri;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * SVGImageView is a View widget that allows users to include SVG images in their layouts.
 * <p>
 * It is implemented as a thin layer over {@code ohos.widget.ImageView}.
 *
 * <h2>XML attributes</h2>
 * <dl>
 *   <dt><code>svg</code></dt>
 *   <dd>A resource reference, or a file name, of an SVG in your application</dd>
 *   <dt><code>css</code></dt>
 *   <dd>Optional extra CSS to apply when rendering the SVG</dd>
 * </dl>
 */
public class SVGImageView extends Component implements Component.DrawTask {
    private static final HiLogLabel LOG_LABEL = new HiLogLabel(HiLog.LOG_APP,
            0x101,
            SVGImageView.class.getSimpleName());

    private final RenderOptions mRenderOptions = new RenderOptions();
    private Picture mPictureToDraw = new Picture();
    private SVG svg = null;
    private SVG svgg = null;

    public SVGImageView(Context context) {
        super(context);
    }

    public SVGImageView(Context context, AttrSet attrs) {
        super(context, attrs, 0);
        init(attrs, 0);
    }

    public SVGImageView(Context context, AttrSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttrSet attrs, int defStyle) {
        if (attrs != null) {
            // Check for css attribute
            if (attrs.getAttr("css").isPresent()) {
                String css = attrs.getAttr("css").get().getStringValue();
                mRenderOptions.css(css);
            }
            // Check whether svg attribute is a string.
            // Could be a URL/filename or an SVG itself
            if (attrs.getAttr("svg").isPresent()) {
                String url = attrs.getAttr("svg").get().getStringValue();
                Uri uri = Uri.parse(url);
                if (internalSetImageURI(uri)) {
                    return;
                }

                // Not a URL, so try loading it as an asset filename
                if (internalSetImageAsset(url)) {
                    return;
                }

                // Last chance, maybe there is an actual SVG in the string
                // If the SVG is in the string, then we will assume it is not very large, and thus doesn't need to be parsed in the background.
                setFromString(url);
            }
            if (attrs.getAttr("svg_id").isPresent()) {
                int resId = attrs.getAttr("svg_id").get().getIntegerValue();
                if (resId != -1) {
                    setImageResource(resId);
                }
            }
        } else {
            HiLog.error(LOG_LABEL, "attrSet = NULL");
        }
    }

    /**
     * Directly set the SVG that should be rendered by this view.
     *
     * @param svg An {@code SVG} instance
     * @since 1.2.1
     */
    public void setSVG(SVG svg) {
        if (svg == null) {
            throw new IllegalArgumentException(" Null value passed to setSVG()");
        }
        this.svg = svg;
        doRender();
    }

    /**
     * Directly set the SVG and the CSS.
     *
     * @param svg An {@code SVG} instance
     * @param css Optional extra CSS to apply when rendering
     * @since 1.3
     */
    public void setSVG(SVG svg, String css) {
        if (svg == null) {
            throw new IllegalArgumentException(" Null value passed to setSVG()");
        }
        this.svg = svg;
        this.mRenderOptions.css(css);

        doRender();
    }

    /**
     * Directly set the CSS.
     *
     * @param css Extra CSS to apply when rendering
     * @since 1.3
     */
    public void setCSS(String css) {
        this.mRenderOptions.css(css);
        doRender();
    }

    /**
     * Load an SVG image from the given resource id.
     *
     * @param resourceId the id of an Ohos resource in your application
     */
    // @Override
    public void setImageResource(int resourceId) {
        Context context = getContext();
        SvgEventHanlder svgEventHanlder = new SvgEventHanlder(EventRunner.current());
        TaskDispatcher globalTaskDispatcher = getContext().getGlobalTaskDispatcher(TaskPriority.HIGH);
        globalTaskDispatcher.asyncDispatch(() -> {
            try {
                svgg = SVG.getFromResource(context, resourceId);
            } catch (SVGParseException | IOException | NotExistException e) {
                HiLog.error(LOG_LABEL, e.getMessage());
            }

            InnerEvent event = InnerEvent.get(SvgEventHanlder.EVENT_ID_DO_RENDER, 0, svgg);
            svgEventHanlder.sendEvent(event, 0, EventHandler.Priority.IMMEDIATE);
        });
    }

    /**
     * Load an SVG image from the given resource URI.
     *
     * @param uri the URI of an Ohos resource in your application
     */
    // @Override
    public void setImageURI(Uri uri) {
        if (!internalSetImageURI(uri)) {
            HiLog.error(LOG_LABEL, "File not found in uri: " + uri);
        }
    }

    /**
     * Load an SVG image from the given asset filename.
     *
     * @param filename the file name of an SVG in the assets folder in your application
     */
    public void setImageAsset(String filename) {
        if (!internalSetImageAsset(filename)) {
            HiLog.error(LOG_LABEL, "File not found in Asset " + filename);
        }
    }

    /*
     * Attempt to set a picture from a Uri. Return true if it worked.
     */
    private boolean internalSetImageURI(Uri uri) {
        try {
            new Ability().openFile(uri, "rw");
            InputStream is = DataAbilityHelper.creator(getContext()).obtainInputStream(uri);
            SvgEventHanlder svgEventHanlder = new SvgEventHanlder(EventRunner.current());
            TaskDispatcher globalTaskDispatcher = getContext().getGlobalTaskDispatcher(TaskPriority.HIGH);
            globalTaskDispatcher.asyncDispatch(() -> {
                try {
                    svgg = SVG.getFromInputStream(is);
                } catch (SVGParseException e) {
                    HiLog.error(LOG_LABEL, "Parse error loading URI: " + e.getMessage());
                } finally {
                    try {
                        is.close();
                    } catch (IOException e) { /* do nothing */ }
                }

                InnerEvent event = InnerEvent.get(SvgEventHanlder.EVENT_ID_DO_RENDER, 0, svgg);
                svgEventHanlder.sendEvent(event, 0, EventHandler.Priority.IMMEDIATE);
            });
            return true;
        } catch (FileNotFoundException | DataAbilityRemoteException e) {
            return false;
        }
    }

    private boolean internalSetImageAsset(String filename) {
        try {
            InputStream is = getContext().getResourceManager().getRawFileEntry("resources/rawfile/"
                    + filename).openRawFile();
            SvgEventHanlder svgEventHanlder = new SvgEventHanlder(EventRunner.current());
            TaskDispatcher globalTaskDispatcher = getContext().getGlobalTaskDispatcher(TaskPriority.HIGH);
            globalTaskDispatcher.asyncDispatch(() -> {
                try {
                    svgg = SVG.getFromInputStream(is);
                } catch (SVGParseException e) {
                    HiLog.error(LOG_LABEL, "Parse error loading URI: " + e.getMessage());
                } finally {
                    try {
                        is.close();
                    } catch (IOException e) { /* do nothing */ }
                }

                InnerEvent event = InnerEvent.get(SvgEventHanlder.EVENT_ID_DO_RENDER, 0, svgg); // 2 means
                svgEventHanlder.sendEvent(event, 0, EventHandler.Priority.IMMEDIATE);
            });
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    private void setFromString(String url) {
        try {
            this.svg = SVG.getFromString(url);
            doRender();
        } catch (SVGParseException e) {
            // Failed to interpret url as a resource, a filename, or an actual SVG...
            HiLog.error(LOG_LABEL, "Could not find SVG at: " + url);
        }
    }

    private void doRender() {
        if (svg == null) {
            return;
        }
        mPictureToDraw.beginRecording(mPictureToDraw.getWidth(), mPictureToDraw.getHeight());
        mPictureToDraw = svg.renderToPicture(mRenderOptions);
        mPictureToDraw.endRecording();
        addDrawTask(this);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        float widthHeightRatio = (float) mPictureToDraw.getWidth() / (float) mPictureToDraw.getHeight();
        float thisWidth = this.getWidth();
        float thisHeight = this.getHeight();
        float tarWidth;
        float tarHeight;
        if (thisWidth <= thisHeight) {
            tarHeight = Math.min((thisWidth / widthHeightRatio), thisHeight);
            tarWidth = ((thisWidth / widthHeightRatio) > thisHeight) ? (thisHeight * widthHeightRatio) : thisWidth;
        } else {
            tarWidth = (Math.min(thisHeight * widthHeightRatio, thisWidth));
            tarHeight = ((thisHeight * widthHeightRatio > thisWidth) ? (thisWidth / widthHeightRatio) : thisHeight);
        }
        canvas.drawPicture(mPictureToDraw, new RectFloat(0, 0, tarWidth, tarHeight));
    }

    private class SvgEventHanlder extends EventHandler {
        static final int EVENT_ID_DO_RENDER = 0x1;

        SvgEventHanlder(EventRunner runner) {
            super(runner);
        }

        @Override
        protected void processEvent(InnerEvent event) {
            super.processEvent(event);
            int eventId = event.eventId;
            Object object = event.object;
            if (eventId == EVENT_ID_DO_RENDER) {
                SVGImageView.this.svg = (SVG) object;
                doRender();
            }
        }
    }
}
