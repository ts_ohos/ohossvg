/*
   Copyright 2013 Paul LeBeau, Cave Rock Software Ltd.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package com.caverock.ohossvg;

import ohos.agp.text.Font;
import ohos.global.resource.ResourceManager;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.Set;
/**
 * A sample implementation of {@link SVGExternalFileResolver} that retrieves files from
 * an application's "assets" folder.
 */
public class SimpleAssetResolver extends SVGExternalFileResolver {
    private static final String TAG = "SimpleAssetResolver_zz";
    static final HiLogLabel LABELI = new HiLogLabel(HiLog.DEBUG, 0x00201, TAG); // MY_MODULE=0x00201
    private static final Set<String> SUPPORTEDFORMATS = new HashSet<>(8);
    private static final int BUFFER = 4096;
    private ResourceManager assetManager;

    @SuppressWarnings({"WeakerAccess", "unused"})
    public SimpleAssetResolver(ResourceManager assetManager) {
        super();
        this.assetManager = assetManager;
    }

    // Static initialiser
    static {
        // PNG, JPEG and SVG are required by the SVG 1.2 spec
        SUPPORTEDFORMATS.add("image/svg+xml");
        SUPPORTEDFORMATS.add("image/jpeg");
        SUPPORTEDFORMATS.add("image/png");

        SUPPORTEDFORMATS.add("image/pjpeg");
        SUPPORTEDFORMATS.add("image/gif");
        SUPPORTEDFORMATS.add("image/bmp");
        SUPPORTEDFORMATS.add("image/x-windows-bmp");
        SUPPORTEDFORMATS.add("image/webp");
    }

    /**
     * Attempt to find the specified font in the "assets" folder and return a Typeface object.
     * For the font name "Foo", first the file "Foo.ttf" will be tried and if that fails, "Foo.otf".
     */
    @Override
    public Font resolveFont(String fontFamily, float fontWeight, String fontStyle, float fontStretch) {
        HiLog.info(LABELI,
                "resolveFont('" + fontFamily + "'," + fontWeight + ",'" + fontStyle + "'," + fontStretch + ") ");

        // Try font name with suffix ".ttf"
        try {
            return new Font.Builder(fontFamily + ".ttf").build();
        } catch (RuntimeException ignored) {
        }

        // That failed, so try ".otf"
        try {
            return new Font.Builder(fontFamily + ".otf").build();
        } catch (RuntimeException e) {
            /* do nothing with this RuntimeException */
        }

        Font.Builder builder = new Font.Builder(fontFamily + ".ttc");

        // Get the first font file in the collection
        return builder.build();
    }

    /**
     * Attempt to find the specified image file in the <code>assets</code> folder and return a decoded Bitmap.
     */
    @Override
    public PixelMap resolveImage(String filename) {
        HiLog.info(LABELI, "resolveImage(" + filename + ")");
        try {
            InputStream istream = assetManager.getRawFileEntry(filename).openRawFile();
            return ImageSource.create(istream,
                    new ImageSource.SourceOptions()).createPixelmap(new ImageSource.DecodingOptions());
        } catch (Exception e1) {
            return null;
        }
    }

    /**
     * Returns true when passed the MIME types for SVG, JPEG, PNG or any of the
     * other bitmap image formats supported.
     */
    @Override
    public boolean isFormatSupported(String mimeType) {
        return SUPPORTEDFORMATS.contains(mimeType);
    }

    /**
     * Attempt to find the specified stylesheet file in the "assets" folder and return its string contents.
     *
     * @since 1.3
     */
    @Override
    public String resolveCSSStyleSheet(String url) {
        HiLog.info(LABELI, "resolveCSSStyleSheet(" + url + ")");
        return getAssetAsString(url);
    }

    /*
     * Read the contents of the asset whose name is given by "url" and return it as a String.
     */
    private String getAssetAsString(String url) {
        InputStream is = null;

        // noinspection TryFinallyCanBeTryWithResources
        try {
            is = assetManager.getRawFileEntry(url).openRawFile();

            // noinspection CharsetObjectCanBeUsed
            Reader r = new InputStreamReader(is, Charset.forName("UTF-8"));
            char[] buffer = new char[BUFFER];
            StringBuilder sb = new StringBuilder();
            int len = r.read(buffer);
            while (len > 0) {
                sb.append(buffer, 0, len);
                len = r.read(buffer);
            }
            return sb.toString();
        } catch (IOException e) {
            return null;
        } finally {
            try {
                if (is != null) {
                    is.close();
                }
            } catch (IOException e) {
                // Do nothing
            }
        }
    }
}
